import argparse
import png
import array
import subprocess

class Converter():

	def __init__(self, options):
		
		# this image named spoil.png will contain the text message
		reader = png.Reader(filename='spoil.png')
		width, h, self.pixels, self.metadata = reader.read_flat()
		self.options = options
		print(options)

		
				# the text that is inserted, it can be found in the header of the variable plaintext

	def run(self):
		if self.metadata['alpha']:

			if self.options.write:
				if self.options.text :
					text = self.options.text 
					if len(text) <= len(self.pixels):
						for i in range(len(text)):
							indice = i*8     # 8 byte => 2 self.pixels
							self.pixels[indice:indice+8] = self.encode(text[i], self.pixels[indice:indice+8])
					else:
						print("the size of the text is too long")

				elif self.options.file:
					global_indice = 0
					size_file = 0
					for line in open(self.options.file).readlines():
						size_file += len(line)
						if size_file <= len(self.pixels):
							for i in range(len(line)):
								indice = global_indice*8
								self.pixels[indice:indice+8] = self.encode(line[i], self.pixels[indice:indice+8])
								global_indice +=1
						else: 
							print("the size of the file is too long")

				# the text message  is hidded inside the image spoil3.png 
				new_pixels = [int(row) for row in self.pixels]
				output = open('spoil3.png', 'wb')
				writer = png.Writer(3200, 800)
				writer.write_array(output, new_pixels)
				

			else:
				# it read the last image spoil3 that contain the message and extract it
				reader = png.Reader(filename='spoil3.png')
				width, h, pixels, metadata = reader.read_flat()
				text = self.decode(pixels)
				text_sequence = [text[i:i+50000] for i in range(0, len(text), 50000)]
				# the return message to known : text it is soo long => hence the need to cut it into blocks.  
				# and apply strings on each block
				plaintext = ""
				for block in text_sequence:
					plaintext+= self.get_plaintext(block)
				print(plaintext)

	def get_plaintext(self, garbage):
		return subprocess.check_output("echo %s | strings" % garbage, shell=True, universal_newlines=True)

	def encode(self, caracter, pixels):
		binaire_caracter = self.padding(bin(ord(caracter))[2:])
		new_pixels = []
		for i in range(len(pixels)):
			binaire_pixel = self.padding(bin(pixels[i])[2:])
			new_pixel = binaire_pixel[:7]+binaire_caracter[i]
			new_pixels.append(int(new_pixel,2))
		return array.array('B', new_pixels)

	def decode(self, pixels):
		extracted =""
		row_byte = [pixels[i:i+8] for i in range(0, len(pixels), 8)]
		final_caracter = ""
		for octet in row_byte:
			bits = ""
			for element in octet:
				bits += self.padding(bin(element)[2:])[7]
			final_caracter += chr(int(bits,2))
		return final_caracter
		

			

	def padding(self, byte):
		if len(byte)<8:
			return (8-(len(byte)))*'0'+str(byte)
		return byte

def run():
	parser = argparse.ArgumentParser()
	parser.add_argument("-f", "--file", help="file text")
	parser.add_argument("-t", "--text", help="text to be embbeded in image")
	parser.add_argument("-w", '--write', action='store_true')
	options = parser.parse_args()

	converter = Converter(options)
	converter.run()


def main():
	run()

main()
